let obj1 = {
  firstName: "nitan",
  lastName: "thapa",
  mergeFirstLasName: function () {
    let fullName = `${this.firstName} ${this.lastName}`;

    return fullName;
  },
};

obj1.mergeFirstLasName();

let keysArray = Object.keys(obj1);
let values = Object.values(obj1);

console.log(keysArray);
// console.log(values);

// let fullName = `${obj1.firstName} ${obj1.lastName}`;

// console.log(fullName);
