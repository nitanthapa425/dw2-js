let fun = (a, b = 2, c = 9) => {
  let sum = a + b + c;

  return sum;
};

let result = fun(5);

console.log(result);
