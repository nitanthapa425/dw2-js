let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "other",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "other",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "other",
    },
  },
];

// find those array of  title whose price is >= 3000

let filterOptput = products.filter((value, i) => {
  if (value.price >= 3000) return true;
  else return false;
});

let desiredOptput = filterOptput.map((value, i) => {
  return value.title;
});

console.log(desiredOptput);
